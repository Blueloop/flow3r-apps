flow3r apps - <https://flow3r.garden/apps>
==========================================
This repository is the index of flow3r apps.  If you want to publish your app,
you are in the right place!

How to publish your app
-----------------------
1. Create a repository on <https://git.flow3r.garden/> with your app.  In the
   repository root, there needs to be a `flow3r.toml` file with metadata for
   your app.  Check [`flow3r.toml` Contents](#flow3r.toml-contents) below for
   details.
2. Click this link to create a new file in this app index for your app:
   <https://git.flow3r.garden/-/ide/project/flow3r/flow3r-apps/edit/main/-/apps/>
3. _Filename_ should be `your-app-name.toml`
4. Content of the file is the name of your repository:
   ```toml
   repo = "<your-user-handle>/<app-repo-name>"
   ```
   For example, if your repository is `https://git.flow3r.garden/rahix/flow3r-field`, the file should contain
   ```toml
   repo = "rahix/flow3r-field"
   ```
5. Set commit message to `Add <your-app-name>`.
6. Click **Commit Changes**.
7. On the next page, click **Create merge request**.
8. You're done!  We will merge the merge request shortly and then your app will be visible in the index.

How to publish app updates
--------------------------
If you want to advertise a new version of your app, push a commit where you
increase the `metadata.version` field in `flow3r.toml`.  The commit where the
`version` field is incremented is going to be published as the new version.

`flow3r.toml` Contents
----------------------
```toml
[app]
# The name of your app as displayed in the menu
name = "Rahix' Nick"

# The submenu where your app should appear
menu = "Badge"

[entry]
# The name of your entry point `Application` class
class = "NickApp"

[metadata]
# Your nickname.  Must be at most 32 characters!
author = "rahix"

# License of your app as an SPDX identifier: <https://spdx.org/licenses/>
license = "LGPL-3.0-only"

# URL to the repository of your app.
url = "https://git.flow3r.garden/flow3r/flow3r-firmware"

# Description of your app.  Maximum 140 characters!
description = "A custom nickname app with more purple and more comic sans."

# Version number of your app.  If you push a commit where this number is
# increased, we interpret this as a new version being released.
#
# Version number must be an integer!
version = 1
```
